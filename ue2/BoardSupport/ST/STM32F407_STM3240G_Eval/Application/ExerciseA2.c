#include "Exercises.h"
#include "RTOS.h"


#define TIME_SLICE_1 1
#define TIME_SLICE_2 1
#define TIME_SLICE_3 1

#define TIMER_PERIOD 1000


static OS_STACKPTR int Stack1[128], Stack2[128], Stack3[128]; // Task stacks
static OS_TASK         TCB1, TCB2, TCB3;                      // Task control blocks
static OS_TIMER timer;

static char flag;


static void timerFunc(void) {
    flag = 1;
    OS_TIMER_Restart(&timer);
}


static void Task1(void) {
    while (1) {
    }
}

static void Task2(void) {
    while (1) {
    }
}

static void Task3(void) {
    static unsigned int counter = 0;

    while (1) {
        if (flag == 1) {
            counter = 0;
            flag = 0;
        } else {
            counter++;
        }
    }
}


void ExerciseA2_Init(void) {
    OS_TIMER_Create(&timer, timerFunc, TIMER_PERIOD);
    OS_TIMER_Start(&timer);

    OS_TASK_Create(&TCB1, "Task1", 50, Task1, Stack1, sizeof(Stack1), TIME_SLICE_1);
    OS_TASK_Create(&TCB2, "Task2", 50, Task2, Stack2, sizeof(Stack2), TIME_SLICE_2);
    OS_TASK_Create(&TCB3, "Task3", 50, Task3, Stack3, sizeof(Stack3), TIME_SLICE_3);
}
