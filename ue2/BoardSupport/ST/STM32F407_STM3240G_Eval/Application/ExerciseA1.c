#include "Exercises.h"
#include "RTOS.h"


#define TIME_SLICE_1 100
#define TIME_SLICE_2 50
#define TIME_SLICE_3 10


static OS_STACKPTR int Stack1[128], Stack2[128], Stack3[128]; // Task stacks
static OS_TASK         TCB1, TCB2, TCB3;                      // Task control blocks


static void Task1(void) {
    while (1) {
        (void) 1;
    }
}

static void Task2(void) {
    while (1) {
        (void) 2;
    }
}

static void Task3(void) {
    while (1) {
        (void) 3;
    }
}


void ExerciseA1_Init(void) {
    OS_TASK_Create(&TCB1, "Task1", 50, Task1, Stack1, sizeof(Stack1), TIME_SLICE_1);
    OS_TASK_Create(&TCB2, "Task2", 50, Task2, Stack2, sizeof(Stack2), TIME_SLICE_2);
    OS_TASK_Create(&TCB3, "Task3", 50, Task3, Stack3, sizeof(Stack3), TIME_SLICE_3);
}
