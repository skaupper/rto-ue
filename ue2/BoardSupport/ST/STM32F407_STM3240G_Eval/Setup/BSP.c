#include "BSP.h"

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/

#define GPIOA_BASE_ADDR           ((unsigned int)0x40020000)

/****** LED port assignement on MB786 eval board ********************/

#define GPIOA_MODER               (*(volatile unsigned int*)(GPIOA_BASE_ADDR + 0x00))
#define GPIOA_ODR                 (*(volatile unsigned int*)(GPIOA_BASE_ADDR + 0x14))
#define GPIOA_BSRR                (*(volatile unsigned int*)(GPIOA_BASE_ADDR + 0x18))

/****** SFRs used for LED-Port **************************************/

#define RCC_BASE_ADDR             ((unsigned int)(0x40023800))
#define RCC_AHB1RSTR              (*(volatile unsigned int*)(RCC_BASE_ADDR + 0x10))
#define RCC_AHB1ENR               (*(volatile unsigned int*)(RCC_BASE_ADDR + 0x30))

#define RCC_LEDPORT_RSTR          RCC_AHB1RSTR
#define RCC_LEDPORT_ENR           RCC_AHB1ENR
#define RCC_LEDPORT_BIT           (0)

/****** Assign LEDs to Ports ****************************************/

#define LED_PORT_MODER            GPIOA_MODER
#define LED_PORT_ODR              GPIOA_ODR
#define LED_PORT_BSRR             GPIOA_BSRR

#define LED0_BIT                  (5)

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*       BSP_Init()
*/
void BSP_Init(void) {
    //
    // Initialize port for LEDs (sample application)
    //
    RCC_LEDPORT_ENR  &= ~(1uL << RCC_LEDPORT_BIT);
    RCC_LEDPORT_RSTR &= ~(1uL << RCC_LEDPORT_BIT);
    RCC_LEDPORT_ENR  |=  (1uL << RCC_LEDPORT_BIT);

    LED_PORT_MODER   &= ~(3uL << (LED0_BIT * 2)); // Reset mode; sets port to input
    LED_PORT_MODER   |=  (1uL << (LED0_BIT * 2)); // Set to output mode
    LED_PORT_BSRR     =  (0x10000uL << LED0_BIT); // Initially clear LEDs
}

/*********************************************************************
*
*       BSP_SetLED()
*/
void BSP_SetLED(void) {
    LED_PORT_BSRR = (1uL << LED0_BIT);       // Switch on LED0
}

/*********************************************************************
*
*       BSP_ClrLED()
*/
void BSP_ClrLED(void) {
    LED_PORT_BSRR = (0x10000uL << LED0_BIT); // Switch off LED0
}

/*********************************************************************
*
*       BSP_ToggleLED()
*/
void BSP_ToggleLED(void) {
    if ((LED_PORT_ODR & (1uL << LED0_BIT)) == 0) {  // LED is switched off
        LED_PORT_BSRR = (1uL << LED0_BIT);            // Switch on LED0
    } else {
        LED_PORT_BSRR = (0x10000uL << LED0_BIT);      // Switch off LED0
    }
}

/****** End Of File *************************************************/
