#include "debug.h"


void DEBUG_Init(void)
{
    RCC_AHBPeriphClockCmd(DEBUG_RCC_PORT, ENABLE);

    // shifted by 16 to reset the pins instead of set
    GPIOB->BSRR = (uint32_t)(SYSTICK_PIN | TASK_COUNTER_PIN | TASK_KEY_PIN | TASK_LED_PIN
                             | TASK_WATCH_PIN | TASK_POTI_PIN | TASK_MANDELBROT_PIN)
                  << 16;

    GPIO_InitTypeDef gpioInit;
    GPIO_StructInit(&gpioInit);

    gpioInit.GPIO_Mode = GPIO_Mode_OUT;
    gpioInit.GPIO_OType = GPIO_OType_PP;
    gpioInit.GPIO_Pin = (SYSTICK_PIN | TASK_COUNTER_PIN | TASK_KEY_PIN | TASK_LED_PIN
                         | TASK_WATCH_PIN | TASK_POTI_PIN | TASK_MANDELBROT_PIN);

    GPIO_Init(DEBUG_PORT, &gpioInit);
}


// DEBUG_Enter and DEBUG_Exit could be implemented as macros as well to reduce their overhead!

void DEBUG_Enter(debug_pin_t pin)
{
    // set the pin
    DEBUG_PORT->BSRR = pin;
}

void DEBUG_Exit(debug_pin_t pin)
{
    // reset pin
    DEBUG_PORT->BSRR = pin << 16;
}
