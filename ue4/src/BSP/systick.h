#ifndef __TICK_H
#define __TICK_H
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Tick_InitSysTick(void);
uint32_t Tick_GetTicks(void);


#endif /* __TICK_H */
