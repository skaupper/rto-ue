#include "TaskKey.h"
#include "BSP/Key.h"
#include "BSP/TftDisplay.h"
#include "BSP/debug.h"
#include "BSP/systick.h"
#include "StdDef.h"
#include "display_offsets.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>


#define MAX_LEN 3
#define DELAY 30  // Delay after which the task is allowed to run again


static char tmpBuf[MAX_LEN];
static bool entered = false;

static const uint16_t colors[] = {TFT_COLOR_BLUE, TFT_COLOR_GREEN, TFT_COLOR_BLACK};
static const int colorCount = sizeof(colors) / sizeof(uint16_t);


void TaskKey(void)
{
    static uint32_t lastTicks = 0;
    uint32_t currentTicks = Tick_GetTicks();
    // check if the minimum task delay is exceeded
    if (currentTicks - lastTicks < DELAY) {
        return;
    }

    DEBUG_Enter(TASK_KEY_PIN);
    if (!entered) {
        // Draw the Task string only once
        Tft_DrawString(X_POS(0), Y_POS(1), "Key");
        entered = true;
    }


    static BOOL lastButton0 = FALSE;
    static int currentColor = 0;

    BOOL btn0 = Key_GetState(KeyType_USER0);
    BOOL btn1 = Key_GetState(KeyType_USER1);

    if (!lastButton0 && btn0) {
        currentColor = (currentColor + 1) % colorCount;
    }

    Tft_SetForegroundColourRgb16(colors[currentColor]);

    snprintf(tmpBuf, MAX_LEN, "%1d%1d", btn0, btn1);
    Tft_DrawString(X_POS(1), Y_POS(1), tmpBuf);

    Tft_SetForegroundColourRgb16(TFT_COLOR_BLACK);

    lastButton0 = btn0;


    lastTicks = currentTicks;
    DEBUG_Exit(TASK_KEY_PIN);
}
