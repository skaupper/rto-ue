#include "TaskPoti.h"
#include "BSP/Adc.h"
#include "BSP/TftDisplay.h"
#include "BSP/debug.h"
#include "BSP/systick.h"
#include "StdDef.h"
#include "display_offsets.h"
#include <stdbool.h>
#include <stdio.h>

#define MAX_LEN 14
#define DELAY 30

static char tmpBuf[MAX_LEN];
static bool entered = false;


void TaskPoti(void)
{
    static uint32_t lastTicks = 0;
    uint32_t currentTicks = Tick_GetTicks();
    // check if the minimum task delay is exceeded
    if (currentTicks - lastTicks < DELAY) {
        return;
    }


    DEBUG_Enter(TASK_POTI_PIN);
    if (!entered) {
        // Draw the Task string only once
        entered = true;
        Tft_DrawString(X_POS(0), Y_POS(4), "Poti");
    }

    snprintf(tmpBuf, MAX_LEN, "%4d", Adc_GetValue(0));
    Tft_DrawString(X_POS(1), Y_POS(4), tmpBuf);

    lastTicks = currentTicks;
    DEBUG_Exit(TASK_POTI_PIN);
}
