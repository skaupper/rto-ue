#include "TaskLed.h"
#include "BSP/Led.h"
#include "BSP/TftDisplay.h"
#include "BSP/debug.h"
#include "BSP/systick.h"
#include "StdDef.h"
#include "display_offsets.h"
#include <stdbool.h>

#define DELAY 95  // Delay after which the task is allowed to run again

static bool entered = false;

static const LEDType Leds[] = {LEDType_LED3, LEDType_LED4, LEDType_LED5};
static const int LedCount = sizeof(Leds) / sizeof(LEDType);
static uint8_t currentLed = 0;


void TaskLed(void)
{
    static uint32_t lastTicks = 0;
    uint32_t currentTicks = Tick_GetTicks();
    // check if the minimum task delay is exceeded
    if (currentTicks - lastTicks < DELAY) {
        return;
    }

    DEBUG_Enter(TASK_LED_PIN);
    if (!entered) {
        // Draw the Task string only once
        entered = true;
        Tft_DrawString(X_POS(0), Y_POS(2), "Led");
    }

    for (uint32_t i = 0; i < LedCount; i++) {
        if (i == currentLed) {
            Led_TurnOn(Leds[i]);
        } else {
            Led_TurnOff(Leds[i]);
        }
    }
    currentLed = (currentLed + 1) % LedCount;


    lastTicks = currentTicks;
    DEBUG_Exit(TASK_LED_PIN);
}
