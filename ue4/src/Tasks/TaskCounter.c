#include "TaskCounter.h"
#include "BSP/TftDisplay.h"
#include "BSP/debug.h"
#include "StdDef.h"
#include "display_offsets.h"
#include <stdbool.h>
#include <stdio.h>

#define MAX_LEN 3  // Maximale Anzahl Zeichen pro Zeile (+1)
static char tmpBuf[MAX_LEN];
static bool entered = false;
static uint32_t counter = 0;


void TaskCounter(void)
{
    DEBUG_Enter(TASK_COUNTER_PIN);
    if (!entered) {
        // Draw the Task string only once
        entered = true;
        Tft_DrawString(X_POS(0), Y_POS(0), "Cnt");
    }

    counter++;
    snprintf(tmpBuf, MAX_LEN, "%d", counter);
    Tft_DrawString(X_POS(1), Y_POS(0), tmpBuf);

    DEBUG_Exit(TASK_COUNTER_PIN);
}
