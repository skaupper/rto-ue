#include "TaskWatch.h"
#include "BSP/TftDisplay.h"
#include "BSP/debug.h"
#include "BSP/systick.h"
#include "StdDef.h"
#include "display_offsets.h"
#include <stdbool.h>
#include <stdio.h>

#define MAX_LEN 20
#define DELAY 900

static char tmpBuf[MAX_LEN];
static bool entered = false;


void TaskWatch(void)
{
    static uint32_t lastTicks = 0;
    uint32_t currentTicks = Tick_GetTicks();
    // check if the minimum task delay is exceeded
    if (currentTicks - lastTicks < DELAY) {
        return;
    }

    DEBUG_Enter(TASK_WATCH_PIN);
    if (!entered) {
        // Draw the Task string only once
        Tft_DrawString(X_POS(0), Y_POS(3), "Watch");
        entered = true;
    }

    uint32_t milliSeconds = 0;
    uint32_t seconds = 0;
    uint32_t minutes = 0;

    // calculate the runtime based on the amount of ticks (ms) since startup
    milliSeconds = Tick_GetTicks();
    seconds = milliSeconds / 1000;
    minutes = seconds / 60;
    seconds -= minutes * 60;
    snprintf(tmpBuf, MAX_LEN, "%02d:%02d", minutes, seconds);
    Tft_DrawString(X_POS(1), Y_POS(3), tmpBuf);

    lastTicks = currentTicks;
    DEBUG_Exit(TASK_WATCH_PIN);
}
