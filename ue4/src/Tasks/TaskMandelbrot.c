#include "TaskMandelbrot.h"
#include "BSP/TftDisplay.h"
#include "BSP/debug.h"
#include "BSP/systick.h"
#include "StdDef.h"
#include "display_offsets.h"
#include <stdbool.h>

// Quelle Algorithmus Mandelbrot: http://warp.povusers.org/Mandelbrot/

#define ImageHeight 150
#define ImageWidth 150
#define OFFSET 150

#define MAX_RUN_TIME 5

static bool entered = false;


static void MandelBrot(void)
{
    // Constants
    double const MinRe = -2.0;
    double const MaxRe = 1.0;
    double const MinIm = -1.2;
    double const MaxIm = MinIm + (MaxRe - MinRe) * ImageHeight / ImageWidth;
    double const Re_factor = (MaxRe - MinRe) / (ImageWidth - 1);
    double const Im_factor = (MaxIm - MinIm) / (ImageHeight - 1);
    unsigned const MaxIterations = 30;

    // Variables which are shared between calls
    static unsigned y = 0;
    static unsigned x = 0;
    static double c_im;
    static double c_re;
    static double Z_re;
    static double Z_im;
    static unsigned char isInside;
    static unsigned n;
    static double Z_re2;
    static double Z_im2;

    // When did the current call start?
    uint64_t entryTicks = Tick_GetTicks();

    for (; y < ImageHeight; y++) {
        c_im = MaxIm - y * Im_factor;

        for (; x < ImageWidth; x++) {
            if (n == 0) {
                c_re = MinRe + x * Re_factor;
                Z_re = c_re, Z_im = c_im;
                isInside = TRUE;
            }
            for (; n < MaxIterations; ++n) {
                Z_re2 = Z_re * Z_re, Z_im2 = Z_im * Z_im;
                if (Z_re2 + Z_im2 > 4) {
                    isInside = FALSE;
                    break;
                }
                Z_im = 2 * Z_re * Z_im + c_im;
                Z_re = Z_re2 - Z_im2 + c_re;

                // Did we already exceed the maximum allowed run time?
                if (Tick_GetTicks() - entryTicks > MAX_RUN_TIME) {
                    return;
                }
            }
            n = 0;

            if (isInside) {
                Tft_DrawPixel(y, x + OFFSET);
            }
        }

        x = 0;
    }

    y = 0;
}


void TaskMandelbrot(void)
{
    DEBUG_Enter(TASK_MANDELBROT_PIN);
    if (!entered) {
        // Draw the Task string only once
        entered = true;
        Tft_DrawString(X_POS(0), Y_POS(5), "ManBr");
    }

    MandelBrot();
    DEBUG_Exit(TASK_MANDELBROT_PIN);
}
