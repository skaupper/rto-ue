#ifndef DEBUG_H
#define DEBUG_H

#include <stdint.h>
#include "stm32f0xx_gpio.h"

typedef uint16_t debug_pin_t;


#define DEBUG_PORT GPIOB
#define DEBUG_RCC_PORT RCC_AHBPeriph_GPIOB


#define SYSTICK_PIN         GPIO_Pin_0
#define TASK_COUNTER_PIN    GPIO_Pin_1
#define TASK_KEY_PIN        GPIO_Pin_3
#define TASK_LED_PIN        GPIO_Pin_12
#define TASK_WATCH_PIN      GPIO_Pin_13
#define TASK_POTI_PIN       GPIO_Pin_14
#define TASK_MANDELBROT_PIN GPIO_Pin_15


void DEBUG_Init(void);

void DEBUG_Enter(debug_pin_t pin);
void DEBUG_Exit(debug_pin_t pin);


#endif
