#ifndef DISPLAY_OFFSETS_H
#define DISPLAY_OFFSETS_H

#define X_OFF 10
#define Y_OFF 18

#define X_INCR 120
#define Y_INCR 24

// for easier handling provide macros for the user
#define X_POS(col) (X_OFF + col * X_INCR)
#define Y_POS(row) (Y_OFF + row * Y_INCR)

#endif
