#include "APOS/APOS.h"
#include "BSP/Led.h"
#include "stdbool.h"
//
// prepare task information (stacks and TCBs)
//

#define STACK_SIZE_A 128
#define STACK_SIZE_B 128
#define STACK_SIZE_C 128

static char StackA[STACK_SIZE_A];
static char StackB[STACK_SIZE_B];
static char StackC[STACK_SIZE_C];

static APOS_TCB_STRUCT pTaskA;
static APOS_TCB_STRUCT pTaskB;
static APOS_TCB_STRUCT pTaskC;

//
// Prototypes for assembler procedures
//

void FillTaskA(void);
void FillTaskB(void);
void FillTaskC(void);



//
// Tasks
//

static void TaskA(void)
{
    FillTaskA();
    while(1)
    {
    }
}

static void TaskB(void)
{
    FillTaskB();
    while(1)
    {
    }
}

static void TaskC(void)
{
    FillTaskC();
    while(1)
    {
    }
}



void InitTasks(void) 
{
    APOS_TASK_Create(&pTaskA, "TaskA", 0, TaskA, StackA, STACK_SIZE_A, 0);
    APOS_TASK_Create(&pTaskB, "TaskB", 0, TaskB, StackB, STACK_SIZE_B, 0);
    APOS_TASK_Create(&pTaskC, "TaskC", 0, TaskC, StackC, STACK_SIZE_C, 0);
}
