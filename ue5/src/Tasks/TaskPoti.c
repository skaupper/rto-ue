#include "BSP/TftDisplay.h"
#include "StdDef.h"
#include "TaskPoti.h"
#include "BSP/debug.h"
#include "APOS/APOS.h"
#include "BSP/Adc.h"
#include <stdio.h>
#include "display_offsets.h"
#define MAX_LEN 14
#define STACKSIZE 256

static APOS_TCB_STRUCT tcb;
static uint32_t stack[STACKSIZE/4];
static char tmpBuf[MAX_LEN];

void initPotiTask()
{
    APOS_TASK_Create(&tcb, "TaskPoti", 0, TaskPoti, stack, STACKSIZE, 0);
}


void TaskPoti(void)
{  
    int32_t adcValue;
    
    Tft_DrawString(X_POS(0), Y_POS(4), "Poti");     
    
    while(1)
    {            
        DEBUG_Enter(TASK_POTI_PIN);
        
        adcValue = Adc_GetValue(0);
        snprintf(tmpBuf, MAX_LEN, "%4d", adcValue);
        Tft_DrawString(X_POS(1), Y_POS(4), tmpBuf);  
        
        DEBUG_Exit(TASK_POTI_PIN);
        
        APOS_Delay(30);    
    }
}
