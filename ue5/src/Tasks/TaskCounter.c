#include "BSP/TftDisplay.h"
#include "StdDef.h"
#include "TaskCounter.h"
#include <stdio.h>
#include "BSP/debug.h"
#include "APOS/APOS.h"
#include "display_offsets.h"

#define MAX_LEN 14        // Maximale Anzahl Zeichen pro Zeile
#define STACKSIZE 256
static char tmpBuf[MAX_LEN];
static APOS_TCB_STRUCT tcb;
static uint32_t stack[STACKSIZE/4];



void initCounterTask()
{
    APOS_TASK_Create(&tcb, "TaskCounter", 0, TaskCounter, stack, STACKSIZE, 0);
}


void TaskCounter (void)
{
    static uint32_t counter = 0;
    
    Tft_DrawString(X_POS(0), Y_POS(0), "Cnt ");    
        
    while(1)
    {            
        DEBUG_Enter(TASK_COUNTER_PIN);
        
        counter++;
        snprintf(tmpBuf, MAX_LEN, "%d", counter);
        Tft_DrawString(X_POS(1), Y_POS(0),tmpBuf);   
        
        DEBUG_Exit(TASK_COUNTER_PIN);  
        
			//force taskswitch
        APOS_Delay(0);        
    }   
}
