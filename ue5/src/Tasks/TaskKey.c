#include "BSP/TftDisplay.h"
#include "StdDef.h"
#include "TaskKey.h"
#include "BSP/debug.h"
#include "BSP/Key.h"
#include <stddef.h>
#include <stdio.h>
#include "APOS/APOS.h"
#include "display_offsets.h"

#define STACKSIZE 256
static APOS_TCB_STRUCT tcb;
static uint32_t stack[STACKSIZE/4];



void initKeyTask()
{
	APOS_TASK_Create(&tcb, "TaskKey", 0, TaskKey, stack, STACKSIZE, 0);
}

void TaskKey (void)
{	
    static BOOL lastButton0 = FALSE;
    static BOOL lastButton1 = FALSE;
    static BOOL lastButtonWakeup = FALSE;
    
    Tft_DrawString(X_POS(0), Y_POS(1), "Key");
    
    while(1)
    {    
        DEBUG_Enter(TASK_KEY_PIN);
        
        BOOL btn0 = Key_GetState(KeyType_USER0);
        BOOL btn1 = Key_GetState(KeyType_USER1);
        BOOL btnWakeup = Key_GetState(KeyType_WAKEUP);
        
			//change color according to button pressed
        if (btn0 && !lastButton0) {
            Tft_SetForegroundColourRgb16(TFT_COLOR_RED);
        } else if (btn1 && !lastButton1) {
            Tft_SetForegroundColourRgb16(TFT_COLOR_PURPLE);
        } else if (btnWakeup && !lastButtonWakeup) {
            Tft_SetForegroundColourRgb16(TFT_COLOR_DARK_GREEN);
        }
        
				//detect button presses instead of hold
        lastButton0 = btn0;   
        lastButton1 = btn1;   
        lastButtonWakeup = btnWakeup;
        
        DEBUG_Exit(TASK_KEY_PIN);
        
        APOS_Delay(30);    
    }
}
