#include "BSP/TftDisplay.h"
#include "StdDef.h"
#include "TaskWatch.h"
#include "BSP/debug.h"
#include "APOS/APOS.h"
#include "BSP/systick.h"
#include "display_offsets.h"
#include <stdio.h>

#define STACKSIZE 256
#define MAX_LEN 20
#define DELAY 900

static APOS_TCB_STRUCT tcb;
static uint32_t stack[STACKSIZE/4];

void initWatchTask()
{
    APOS_TASK_Create(&tcb, "TaskWatch", 0, TaskWatch, stack, STACKSIZE, 0);
}



static char tmpBuf[MAX_LEN];

void TaskWatch(void)
{
    Tft_DrawString(X_POS(0), Y_POS(3), "Watch");    
       
    uint32_t milliSeconds = 0;
    uint32_t seconds = 0;
    uint32_t minutes = 0;

    while(1)
    {
        DEBUG_Enter(TASK_WATCH_PIN);
        
        // calculate the runtime based on the amount of ticks (ms) since startup
        milliSeconds = Tick_GetTicks();
        seconds = milliSeconds / 1000;
        minutes = seconds / 60;
        seconds -= minutes * 60;
        snprintf(tmpBuf, MAX_LEN, "%02d:%02d", minutes, seconds);
        
        Tft_DrawString(X_POS(1), Y_POS(3), tmpBuf);    
        
        DEBUG_Exit(TASK_WATCH_PIN);
        
        APOS_Delay(900);    
    }    
}
