            AREA    |.text|, CODE, READONLY
                
                
FillTaskA   PROC
            EXPORT  FillTaskA    
                
            ; registers R8-R12 cannot be written with immediates
            MOVS R0, #0xA8
            MOVS R1, #0xA9
            MOVS R2, #0xAA
            MOVS R3, #0xAB
            MOVS R4, #0xAC
                
            MOV  R8,  R0
            MOV  R9,  R1
            MOV  R10, R2
            MOV  R11, R3
            MOV  R12, R4
                
                
            MOVS R0,  #0xA0
            MOVS R1,  #0xA1
            MOVS R2,  #0xA2
            MOVS R3,  #0xA3
            MOVS R4,  #0xA4
            MOVS R5,  #0xA5
            MOVS R6,  #0xA6
            MOVS R7,  #0xA7
                
            BX LR
            
            ENDP
                
FillTaskB   PROC
            EXPORT  FillTaskB    
                
            MOVS R0, #0xB8
            MOVS R1, #0xB9
            MOVS R2, #0xBA
            MOVS R3, #0xBB
            MOVS R4, #0xBC
                
            MOV  R8,  R0
            MOV  R9,  R1
            MOV  R10, R2
            MOV  R11, R3
            MOV  R12, R4
                
                
            MOVS R0,  #0xB0
            MOVS R1,  #0xB1
            MOVS R2,  #0xB2
            MOVS R3,  #0xB3
            MOVS R4,  #0xB4
            MOVS R5,  #0xB5
            MOVS R6,  #0xB6
            MOVS R7,  #0xB7
                
            BX LR
                
            ENDP
                
                
FillTaskC   PROC
            EXPORT  FillTaskC    
                
            MOVS R0, #0xC8
            MOVS R1, #0xC9
            MOVS R2, #0xCA
            MOVS R3, #0xCB
            MOVS R4, #0xCC
                
            MOV  R8,  R0
            MOV  R9,  R1
            MOV  R10, R2
            MOV  R11, R3
            MOV  R12, R4
                
                
            MOVS R0,  #0xC0
            MOVS R1,  #0xC1
            MOVS R2,  #0xC2
            MOVS R3,  #0xC3
            MOVS R4,  #0xC4
            MOVS R5,  #0xC5
            MOVS R6,  #0xC6
            MOVS R7,  #0xC7
                
            BX LR
                
            ENDP
                
                
            END