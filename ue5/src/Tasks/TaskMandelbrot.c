#include "BSP/TftDisplay.h"
#include "StdDef.h"
#include "TaskMandelbrot.h"
#include "BSP/debug.h"
#include "display_offsets.h"
#include <stdbool.h>
#include "APOS/APOS.h"
#include "BSP/systick.h"
// Quelle Algorithmus Mandelbrot: http://warp.povusers.org/Mandelbrot/ 

#define ImageHeight 150
#define ImageWidth  150
#define OFFSET      150


#define STACKSIZE 512
static APOS_TCB_STRUCT tcb;
static uint32_t stack[STACKSIZE/4];

void initMandelbrotTask()
{
    APOS_TASK_Create(&tcb, "TaskMandelbrot", 0, TaskMandelbrot, stack, STACKSIZE, 5);
}


static void MandelBrot(void)
{
    double const MinRe = -2.0;
    double const MaxRe = 1.0;
    double const MinIm = -1.2;
    double const MaxIm = MinIm + (MaxRe - MinRe) * ImageHeight / ImageWidth;
    double const Re_factor = (MaxRe - MinRe) / (ImageWidth - 1);
    double const Im_factor = (MaxIm - MinIm) / (ImageHeight - 1);
    unsigned const MaxIterations = 30;

    for (unsigned y = 0; y < ImageHeight; y++) {
        double c_im = MaxIm - y * Im_factor;

        for (unsigned x = 0; x < ImageWidth; x++) {
            double c_re = MinRe + x * Re_factor;
            double Z_re = c_re, Z_im = c_im;
            unsigned char isInside = TRUE;
            
            for (unsigned n = 0; n < MaxIterations; ++n) {
                double Z_re2 = Z_re * Z_re, Z_im2 = Z_im * Z_im;
                if (Z_re2 + Z_im2 > 4) {
                    isInside = FALSE;
                    break;
                }
                Z_im = 2 * Z_re * Z_im + c_im;
                Z_re = Z_re2 - Z_im2 + c_re;
            }

            if (isInside) {
                Tft_DrawPixel(y, x + OFFSET);
            }
        }
    }
}


void TaskMandelbrot(void)
{    
    Tft_DrawString(X_POS(0), Y_POS(5), "ManBr");   
    
    static uint16_t colors[] = {
        TFT_COLOR_BLUE,
        TFT_COLOR_BLACK        
    };
    static int nrOfColors = sizeof(colors) / sizeof(uint16_t); 
    int currentColor = 0;
    
    while(1)
    {         
        DEBUG_Enter(TASK_MANDELBROT_PIN);
        MandelBrot();
        
        // set the foreground color after every finished run
        Tft_SetForegroundColourRgb16(colors[currentColor]);
        currentColor = (currentColor + 1) % nrOfColors;
        
        DEBUG_Exit(TASK_MANDELBROT_PIN);
    }    
}
