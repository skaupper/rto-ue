#include "BSP/TftDisplay.h"
#include "StdDef.h"
#include "TaskLed.h"
#include "BSP/debug.h"
#include "BSP/systick.h"
#include "BSP/Led.h"
#include "APOS/APOS.h"
#include "display_offsets.h"

#define STACKSIZE 256

static APOS_TCB_STRUCT tcb;
static uint32_t stack[STACKSIZE/4];



void initLedTask()
{
    APOS_TASK_Create(&tcb, "TaskLed", 0, TaskLed, stack, STACKSIZE, 0);
}

void TaskLed (void)
{    
    static uint8_t currentLed = 0;       
    static const LEDType Leds[] = {
        LEDType_LED3, 
        LEDType_LED4, 
        LEDType_LED5
    };
    static const int LedCount = sizeof(Leds) / sizeof(LEDType);       
                        
    Tft_DrawString(X_POS(0), Y_POS(2), "Led");
        
    while(1)
    {   
        DEBUG_Enter(TASK_LED_PIN);
        
        for(uint32_t i = 0; i < LedCount; i++)
        {
            if(i == currentLed)
            {
                Led_TurnOn(Leds[i]);
            }    
            else 
            {
                Led_TurnOff(Leds[i]);
            }                            
        }
        currentLed = (currentLed+1) % LedCount;            
        
        DEBUG_Exit(TASK_LED_PIN);
        
        APOS_Delay(95);    
    }
}
