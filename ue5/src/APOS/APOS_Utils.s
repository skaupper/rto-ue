    AREA    |.text|, CODE, READONLY
               

PendSV_Handler   	PROC
					EXPORT  PendSV_Handler    
					IMPORT	currentTask
					IMPORT  nextTask
					IMPORT  Scheduler_Locked
							
					;disable all interrupts
					CPSID	i
					
					;save registers r4 to r11 to psp
					MRS		r0, psp
					SUBS	r0, #16
					STMIA	r0!,{r4-r7}
					MOV		r4, r8
					MOV		r5, r9
					MOV		r6, r10
					MOV		r7, r11
					SUBS	r0, #32
					STMIA	r0!,{r4-r7}
					SUBS	r0, #16
														
					;update top of stack value into tcb
					LDR	r2, =currentTask
					LDR	r1, [r2]
					STR	r0, [r1]
				
					;get next top of stack value for next task
					LDR	r2, =nextTask
					LDR	r1, [r2]
					LDR	r0, [r1]
							
					;restore registers r4 to r11 from psp
					LDMIA	r0!,{r4-r7}
					MOV		r8, r4
					MOV		r9, r5
					MOV		r10, r6
					MOV		r11, r7
					LDMIA	r0!,{r4-r7}
					MSR		psp, r0
					ISB
										
					LDR		r0, =0xFFFFFFFD		
					;unlock the scheduler
					LDR		r2, =Scheduler_Locked
					MOVS	r3, #0
					STR		r3, [r2]
					;enable interrupts
					CPSIE	i
											
					BX 		r0
					
					ENDP    
					
					ALIGN 	
					END