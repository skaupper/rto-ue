#include "APOS.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "stm32f0xx.h"
#include <stdbool.h>
#include "BSP/debug.h"

// scheduler states
#define LOCKED 1
#define UNLOCKED 0

// size of additional registers
#define R7_TO_R11 32


typedef struct {
    uint32_t R0;
    uint32_t R1;
    uint32_t R2;
    uint32_t R3;
    uint32_t R12;
    uint32_t LR;
    uint32_t PC;
    uint32_t xPSR;
} STACK_FRAME_STRUCT;

void SaveRegisters(void);
void RestoreRegisters(void);



#define MAX_NR_OF_TASKS 10

static APOS_TCB_STRUCT *schedulableTasks[MAX_NR_OF_TASKS] = {0};
static uint8_t nrOfScheduledTasks = 0;

volatile APOS_TCB_STRUCT * currentTask = NULL;
volatile APOS_TCB_STRUCT * nextTask = NULL;
static uint8_t currentTaskIndex = 0;


// scheduler state
volatile int Scheduler_Locked = UNLOCKED;


// idle Task
#define STACKSIZE 68
static uint32_t stack[STACKSIZE/4];
static APOS_TCB_STRUCT idleTcb;

static void IdleTask(void)
{
    while(1);
}




void APOS_Init(void)
{
    APOS_TASK_Create(&idleTcb, "IdleTask", 0, IdleTask, stack, STACKSIZE, 0);  
    nrOfScheduledTasks = 0;
}



void APOS_Start(void)
{
	//set PendSV to lowest and Systick to highest priority
    NVIC_SetPriority(PendSV_IRQn, 0xff);
    NVIC_SetPriority(SysTick_IRQn, 0x00);
    
    assert(nrOfScheduledTasks > 0);
    
	//prepare PSP for first task
    __set_PSP(schedulableTasks[0]->TopOfStack + sizeof(STACK_FRAME_STRUCT));
	//use PSP in thread mode
    __set_CONTROL(0x02);
    __ISB();
    
	//start the fist task
    currentTaskIndex = 0;
    nextTask = schedulableTasks[0];
    schedulableTasks[0]->pRoutine();
}

void APOS_TASK_Create(APOS_TCB_STRUCT* pTask,   // TaskControlBlock
                      const char* pTaskName,    // Task Name – nur für Debug-Zwecke
                      uint32_t Priority,        // Priorität des Tasks (vorerst nicht in Verwendung)
                      void (*pRoutine)(void),   // Startadresse Task (ROM)
                      void* pStack,             // Startadresse Stack des Tasks (RAM)
                      uint32_t StackSize,       // Größe des Stacks
                      uint32_t TimeSlice)       // Time-Slice für Round Robin Scheduling      
{
    //Prepare Stack
    memset(pStack, 0xCD, StackSize);
    
    // set the T bit
    uint32_t xPSR = (1 << 24);
	
	//stackframe as seen in datasheet
    STACK_FRAME_STRUCT frame = {
        .R0 = 0,
        .R1 = 1,
        .R2 = 2,
        .R3 = 3,
        .R12 = 12,
        .LR = 0, 
        .PC = (uint32_t) pRoutine,
        .xPSR = xPSR
    };
    
    void* frameOffset = (char*)pStack + StackSize - sizeof(frame);
    memcpy(frameOffset, &frame, sizeof(frame));
        
    // fill TCB
    pTask->pRoutine = pRoutine;
    pTask->TopOfStack = (uint32_t)frameOffset - R7_TO_R11;
    
    //set Timeslice
    pTask->timeslice = TimeSlice;
    pTask->resetTimeslice = TimeSlice;
    
    // insert task to schedulable tasks list
    schedulableTasks[nrOfScheduledTasks] = pTask;
    nrOfScheduledTasks++;
    assert(nrOfScheduledTasks <= MAX_NR_OF_TASKS);
}



static volatile int regionCount = 0;

void APOS_Scheduler()
{
    if (APOS_TestRegion()) {
        
        // check scheduler state
        APOS_EnterRegion();
        int local_scheduler_locked = Scheduler_Locked;
        if (Scheduler_Locked == UNLOCKED) {
            Scheduler_Locked = LOCKED;
        }
        APOS_LeaveRegion();
        
        if(local_scheduler_locked == UNLOCKED)
        {
            if(nextTask == &idleTcb)
            {
                // in case the Idle task was scheduled, set it as currentTask
                currentTask = nextTask;   
            }
            else
            {
                // normal case: set current task
                currentTask = schedulableTasks[currentTaskIndex];
            }
            
            
            if(currentTask->timeslice > 0)
            {                
                // check if current task still has time
                Scheduler_Locked = UNLOCKED;                
            }        
            else
            {                
                // reset timeslice
                currentTask->timeslice = currentTask->resetTimeslice;            

                int counter = nrOfScheduledTasks+1;
                int tempCurrentIndex = currentTaskIndex;
							
							//check for tasks without active remaining delay
                do
                {
                    tempCurrentIndex = (tempCurrentIndex + 1) % nrOfScheduledTasks;
                    counter--;
                }
                while(schedulableTasks[tempCurrentIndex]->delay > 0 && counter > 0);
                
								//if all tasks have delays schedule the idle task
                if(counter == 0)
                {
                    nextTask = &idleTcb;
                }
                else
                {
                    currentTaskIndex = tempCurrentIndex;
                    nextTask = schedulableTasks[currentTaskIndex];
                }             
								
							//trigger PENDSV
                SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;            
            }
        }            
    }
}

void APOS_EnterRegion(void) 
{
    __disable_irq();
    regionCount++;
    //DEBUG_Enter(SYSTICK_PIN);
    __enable_irq();
}

void APOS_LeaveRegion(void)
{
    __disable_irq();
    assert(regionCount > 0);
    regionCount--;
//    if (regionCount == 0) {
//        DEBUG_Exit(SYSTICK_PIN);
//    }
    __enable_irq();
}

bool APOS_TestRegion(void)
{
    return regionCount == 0;
}

void APOS_Delay(uint32_t ticks)
{        
    nextTask->delay = ticks;
    APOS_Scheduler();
}

//reduces the timeslice and delay of all tasks
void APOS_Tick(void)
{
    if(schedulableTasks[currentTaskIndex]->timeslice > 0)
    {
        schedulableTasks[currentTaskIndex]->timeslice--;
    }
    
    for(int i = 0; i < nrOfScheduledTasks; i++)
    {
        if(schedulableTasks[i]->delay > 0)
        {
            schedulableTasks[i]->delay--;
        }
    }
}
