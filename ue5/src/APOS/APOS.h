#ifndef APOS_H
#define APOS_H
#include <stdint.h>
#include <stdbool.h>

typedef struct {
    uint32_t volatile TopOfStack;
    void (*pRoutine)(void);
    uint32_t delay;
    uint32_t timeslice;
    uint32_t resetTimeslice;
} APOS_TCB_STRUCT;


void APOS_Init(void);                          

void APOS_Start(void);

void APOS_TASK_Create(APOS_TCB_STRUCT* pTask,   // TaskControlBlock
                      const char* pTaskName,    // Task Name – nur für Debug-Zwecke
                      uint32_t Priority,        // Priorität des Tasks (vorerst nicht in Verwendung)
                      void (*pRoutine)(void),   // Startadresse Task (ROM)
                      void* pStack,             // Startadresse Stack des Tasks (RAM)
                      uint32_t StackSize,       // Größe des Stacks
                      uint32_t TimeSlice);      // Time-Slice für Round Robin Scheduling      

void APOS_Scheduler(void);

void APOS_EnterRegion(void);
void APOS_LeaveRegion(void);
bool APOS_TestRegion(void);
                      
void APOS_Delay(uint32_t ticks);
void APOS_Tick(void);									
							

#endif
