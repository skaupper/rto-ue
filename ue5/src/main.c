#include "Services/StdDef.h"
#include "APOS/APOS.h"
#include "Tasks/TestTask.h"
#include "BSP/systick.h"
#include "BSP/debug.h"
#include "BSP/Led.h"
#include "BSP/TftDisplay.h"
#include "BSP/Adc.h"
#include "BSP/Key.h"
#include "Tasks/TaskCounter.h"
#include "Tasks/TaskKey.h"
#include "Tasks/TaskLed.h"
#include "Tasks/TaskMandelbrot.h"
#include "Tasks/TaskPoti.h"
#include "Tasks/TaskWatch.h"
#include "Fonts/TftFont_16x24.h"

extern void Init_MSP(void);

int main(void)
{    
	//This function is called to determine the usage of the master stack
	  Init_MSP();
	
	//initialize hardware
    DEBUG_Init();
		Key_Init();
    Led_Init();
    Tft_Init();
    Tft_SetFont(&TftFont_16x24);
    Tft_ClearScreen();
    Adc_Init(ADC_CHANNEL_POTENTIOMETER);
    Tick_InitSysTick();
	
	//initialize OS
    APOS_Init();
	
	//add Tasks	
		initCounterTask();
		initKeyTask();
		initLedTask();
		initMandelbrotTask();
		initPotiTask();
    initWatchTask();
		
  //start OS
    APOS_Start();
}


/* #define NDEBUG to ignore all asserts */
#define ASSERT_BUFF_SIZE 150u
/**
 * @brief  Reports the name of the source file and the source line number
 *                    where the assert error has occurred.
 * @param    expr: expression that resulted to false
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
*/
void __aeabi_assert(const char * expr, const char * file, int line)
{
    /* User can add his own implementation to report the expression, file name
        and line number, ex:
        printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    volatile uint32_t linev;
    /* Buffer size, because of long file names */
    volatile uint8_t tmpBuf[ASSERT_BUFF_SIZE];

    if(expr != NULL_PTR && file != NULL_PTR)
    {
        for (uint8_t i = 0; i < ASSERT_BUFF_SIZE; i++)
        {
            /* insert expression */
            if((*expr) != '\0')
            {
                tmpBuf[i] = *expr++;
            }
            /* insert filename */
            else if((*file) != '\0')
            {
                tmpBuf[i] = *file++;
            }
        }
    }
    linev = line;

    /* Infinite loop */
    /* Now Debug with debugger (tmpBuf, Linev) */
    while (1);
}
