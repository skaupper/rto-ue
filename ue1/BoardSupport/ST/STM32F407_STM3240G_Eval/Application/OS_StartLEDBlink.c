/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*       (c) 1995 - 2018 SEGGER Microcontroller GmbH                  *
*                                                                    *
*       Internet: segger.com  Support: support_embos@segger.com      *
*                                                                    *
**********************************************************************
*                                                                    *
*       embOS * Real time operating system for microcontrollers      *
*                                                                    *
*       Please note:                                                 *
*                                                                    *
*       Knowledge of this file may under no circumstances            *
*       be used to write a similar product or a real-time            *
*       operating system for in-house use.                           *
*                                                                    *
*       Thank you for your fairness !                                *
*                                                                    *
**********************************************************************
*                                                                    *
*       OS version: 5.02a                                            *
*                                                                    *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------
File    : OS_StartLEDBlink.c
Purpose : embOS sample program running two simple tasks, each toggling
          a LED of the target hardware (as configured in BSP.c).
*/

#include <stdint.h>
#include <stdio.h>

#include "RTOS.h"
#include "BSP.h"


// exercise specific code (tasks etc.)
#include "ExerciseA.h"
#include "ExerciseB.h"
#include "ExerciseC.h"


/*********************************************************************
*
*       main()
*/
int main(void) {
    OS_Init();    // Initialize embOS
    OS_InitHW();  // Initialize required hardware
    BSP_Init();   // Initialize LED ports
    
    // ExerciseA_Init();
    // ExerciseB_Init();
    ExerciseC_Init();
    
    OS_Start();   // Start embOS
    return 0;
}

/*************************** End of file ****************************/
