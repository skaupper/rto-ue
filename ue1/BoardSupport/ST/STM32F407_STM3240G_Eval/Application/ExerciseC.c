#include "ExerciseC.h"

#include "RTOS.h"
#include <stdint.h>
#include <stdio.h>


static OS_STACKPTR int StackTask1[128], StackTask2[128], StackTask3[128]; // Task stacks
static OS_TASK         TCBTask1, TCBTask2, TCBTask3;                      // Task control blocks

// set a constant buffer length used in the tasks below
static const uint32_t BUFFER_LENGTH = 64;

// a global shared variable
static char sharedChar = 0x00;



static void Task1(void)    {
    while (1) {
        sharedChar = 'A';
        OS_TASK_Delay(1000);
    }
}


static void Task2(void)    {
    // in order to use OS_COM_SendString to send a single character
    // this character has to be wrapped in a null terminated string
    // (the newline is added for readability)
    char tmp[3] = "\0\n";

    while (1) {
        if(sharedChar == 'A') {
            tmp[0] = sharedChar;

            OS_COM_SendString(tmp);
            sharedChar = 'B';
        }
        OS_TASK_Delay(300);
    }
}


static void Task3(void)    {
    char buffer[BUFFER_LENGTH];
    uint16_t counter = 0;
    char lastChar = sharedChar;

    while (1) {
        // check if the shared variable has changed
        if(lastChar != sharedChar) {
            lastChar = sharedChar;
            counter++;

            snprintf(buffer, BUFFER_LENGTH, "Counter: %hu, sharedChar: %c\n", counter, sharedChar);
            OS_COM_SendString(buffer);
        }
    }
}


void ExerciseC_Init(void) {
    OS_TASK_CREATE(&TCBTask1, "Task 1", 100, Task1, StackTask1);
    OS_TASK_CREATE(&TCBTask2, "Task 2", 50, Task2, StackTask2);
    OS_TASK_CREATE(&TCBTask3, "Task 3", 10, Task3, StackTask3);
}
