#ifndef EXERCISE_A_H
#define EXERCISE_A_H

// Initialize Exercise A, which creates a task to toggle the LED
void ExerciseA_Init(void);

#endif
