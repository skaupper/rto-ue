#include "ExerciseB.h"

#include <stdint.h>
#include <stdio.h>

#include "RTOS.h"


static OS_STACKPTR int StackMeasure[128], StackLP[128]; // Task stacks
static OS_TASK         TCBMeasure, TCBLP;               // Task control blocks


// this task is used to measure the time taken for OS_STACK_GetTaskStackSpace and a snprintf
// the results get written to the terminal using OS_COM_SendString
static void MeasureTask(void)    {
    static const uint32_t BUFFER_LENGTH = 64;

    OS_TIMING timing;
    char buffer[BUFFER_LENGTH];

    while (1) {
        // measure time for snprintf
        OS_TIME_StartMeasurement(&timing);
        uint32_t stackSpace = OS_STACK_GetTaskStackSpace(NULL);
        snprintf(buffer, BUFFER_LENGTH, "Stack available: %d\n", stackSpace);
        OS_TIME_StopMeasurement(&timing);

        // output available stack
        OS_COM_SendString(buffer);

        // output used stack
        uint32_t stackUsed = OS_STACK_GetTaskStackUsed(NULL);
        snprintf(buffer, BUFFER_LENGTH, "Stack used: %d\n", stackUsed);
        OS_COM_SendString(buffer);

        // output time measured
        uint32_t timeCycles = OS_TIME_GetResult(&timing);
        uint32_t timeUs = OS_TIME_GetResultus(&timing);
        snprintf(buffer, BUFFER_LENGTH, "Time measured %u us, %u cycles\n", timeUs, timeCycles);
        OS_COM_SendString(buffer);

        // output used stack of LPTask
        stackUsed = OS_STACK_GetTaskStackUsed(&TCBLP);
        snprintf(buffer, BUFFER_LENGTH, "LPTask stack used: %d\n", stackUsed);
        OS_COM_SendString(buffer);


        OS_TASK_Delay(2000);
    }
}

// empty task without anything
// used to determine the stack usage of an empty task
static void LPTask(void) {
    while (1) {
    }
}



void ExerciseB_Init(void) {
    OS_TASK_CREATE(&TCBMeasure, "Measure Task", 100, MeasureTask, StackMeasure);
    OS_TASK_CREATE(&TCBLP, "LP Task", 10, LPTask, StackLP);
}
