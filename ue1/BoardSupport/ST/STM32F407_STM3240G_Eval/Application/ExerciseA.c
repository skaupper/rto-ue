#include "ExerciseA.h"

#include "RTOS.h"
#include "BSP.h"


static OS_STACKPTR int StackLED[128]; // Task stacks
static OS_TASK         TCBLED;        // Task control blocks


// toggle a LED every 500ms which results in a frequency of 1 Hz
static void LEDTask(void) {
    while (1) {
        BSP_ToggleLED();
        OS_TASK_Delay(500);
    }
}


// create a single task to toggle the LED. the priority does not matter
void ExerciseA_Init(void) {
    OS_TASK_CREATE(&TCBLED, "LEDTask", 150, LEDTask, StackLED);
}
