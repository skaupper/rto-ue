#ifndef EXERCISE_B_H
#define EXERCISE_B_H

// Initialize Exercise B, which creates a task measuring stack space and time
// as well as an empty task
void ExerciseB_Init(void);

#endif
