#include "RTOS.h"
#include "BSP.h"

#include "Exercises.h"


int main(void) {
    OS_Init();    // Initialize embOS
    OS_InitHW();  // Initialize required hardware
    BSP_Init();   // Initialize LED ports

    //ExerciseB1_Init();
    ExerciseB2_Init();

    OS_Start();   // Start embOS
    return 0;
}
