#include "Exercises.h"

#include "BSP.h"
#include "RTOS.h"

#include <stdbool.h>


#define MB_MENU_SIZE 1
#define MB_MENU_ENTRIES 10
#define MB_LED_SIZE 1
#define MB_LED_ENTRIES 10


static OS_STACKPTR int StackButton[128], StackMenu[128], StackLed[128];
static OS_TASK         TCBButton, TCBMenu, TCBLed;
static OS_MAILBOX      mbMenu, mbLed;
static OS_TIMER        timer;

static char mbMenuBuffer[MB_MENU_SIZE * MB_MENU_ENTRIES];
static char mbLedBuffer[MB_LED_SIZE * MB_LED_ENTRIES];



static void timerFunc(void) {
    BSP_ToggleLED();
    OS_TIMER_Restart(&timer);
}


static void taskButton(void) {
    static const int DELAY = 20;
    static const int HOLD_THRESHOLD = 500;

    state_button_t lastState = BSP_GetButton();
    state_button_t state;

    int pressedFor = 0;
    bool eventSent = false;
    char msg = 0;

    while (1) {
        state = BSP_GetButton();

        if (state == BUTTON_PRESSED && lastState == BUTTON_RELEASED) {
            pressedFor = 0;
            eventSent = false;
        } else if (state == BUTTON_RELEASED && lastState == BUTTON_PRESSED) {
            pressedFor = 0;
            if (!eventSent) {
                eventSent = true;
                OS_MAILBOX_PutBlocked(&mbMenu, &msg);
            }
        } else if (state == BUTTON_PRESSED) {
            pressedFor += DELAY;
        }

        if (pressedFor >= HOLD_THRESHOLD && !eventSent) {
            eventSent = true;
            OS_MAILBOX_PutBlocked(&mbMenu, &msg);
        }

        lastState = state;
        OS_TASK_Delay(DELAY);
    }
}



static void taskMenu(void) {
    static const char NR_OF_STATES = 4;
    static char currentState = 0;
    char msg;

    while (1) {
        OS_MAILBOX_GetBlocked(&mbMenu, &msg);
        currentState = (currentState + 1) % NR_OF_STATES;
        OS_MAILBOX_PutBlocked(&mbLed, &currentState);
    }
}

static void taskLed(void) {
    static char currentState = 0;
    static const int HZ_10 = 100;
    static const int HZ_1  = 1000;

    while (1) {
        switch (currentState) {
            case 0:
                OS_TIMER_Stop(&timer);
                BSP_ClrLED();
                break;

            case 1:
                BSP_SetLED();
                break;

            case 2:
                OS_TIMER_SetPeriod(&timer, HZ_10/2);
                OS_TIMER_Restart(&timer);
                break;

            case 3:
                OS_TIMER_SetPeriod(&timer, HZ_1/2);
                break;

            default:
                break;
        }

        OS_MAILBOX_GetBlocked(&mbLed, &currentState);
    }
}


void ExerciseB2_Init(void) {
    OS_TIMER_Create(&timer, timerFunc, 1);

    OS_MAILBOX_Create(&mbMenu, MB_MENU_SIZE, MB_MENU_ENTRIES, mbMenuBuffer);
    OS_MAILBOX_Create(&mbLed,  MB_LED_SIZE,  MB_LED_ENTRIES,  mbLedBuffer);

    OS_TASK_CREATE(&TCBButton,  "taskButton", 50, taskButton, StackButton);
    OS_TASK_CREATE(&TCBMenu,    "taskMenu",   50, taskMenu,   StackMenu);
    OS_TASK_CREATE(&TCBLed,     "taskLed",    50, taskLed,    StackLed);
}
