#include "Exercises.h"
#include "RTOS.h"

#include "BSP.h"
#include <stdbool.h>


#define BUTTON_EVENT 1
#define MENU_EVENT 1
#define NR_OF_STATES 4


static OS_STACKPTR int StackButton[128], StackMenu[128], StackLed[128];
static OS_TASK         TCBButton, TCBMenu, TCBLed;
static OS_TIMER        timer;


static void timerFunc(void) {
	  BSP_ToggleLED();
	  OS_TIMER_Restart(&timer);
}

static void taskButton(void) {
    static const int DELAY = 20;
    static const int HOLD_THRESHOLD = 500;

    state_button_t lastState = BSP_GetButton();
    state_button_t state;

    int pressedFor = 0;
    bool eventSent = false;

    while (1) {
        state = BSP_GetButton();

        if (state == BUTTON_PRESSED && lastState == BUTTON_RELEASED) {
            pressedFor = 0;
            eventSent = false;
        } else if (state == BUTTON_RELEASED && lastState == BUTTON_PRESSED) {
            pressedFor = 0;
            if (!eventSent) {
                eventSent = true;
                OS_TASKEVENT_Set(&TCBMenu, BUTTON_EVENT);
            }
        } else if (state == BUTTON_PRESSED) {
            pressedFor += DELAY;
        }

        if (pressedFor >= HOLD_THRESHOLD && !eventSent) {
            eventSent = true;
            OS_TASKEVENT_Set(&TCBMenu, BUTTON_EVENT);
        }

        lastState = state;
        OS_TASK_Delay(DELAY);
    }
}

static void taskMenu(void) {
    static char currentState = 0;

    while (1) {
        OS_TASKEVENT_GetBlocked(BUTTON_EVENT);
        currentState = (currentState + 1) % NR_OF_STATES;
        OS_TASKEVENT_Set(&TCBLed, MENU_EVENT);
    }
}

static void taskLed(void) {
    static char currentState = 0;
    static const int HZ_10 = 100;
    static const int HZ_1  = 1000;

    while (1) {
        switch (currentState) {
            case 0:
                OS_TIMER_Stop(&timer);
                BSP_ClrLED();
                break;

            case 1:
                BSP_SetLED();
                break;

            case 2:
                OS_TIMER_SetPeriod(&timer, HZ_10/2);
                OS_TIMER_Restart(&timer);
                break;

            case 3:
                OS_TIMER_SetPeriod(&timer, HZ_1/2);
                break;

            default:
                break;
        }
        OS_TASKEVENT_GetBlocked(MENU_EVENT);
        currentState = (currentState + 1) % NR_OF_STATES;
    }
}


void ExerciseB1_Init(void) {
    OS_TIMER_Create(&timer, timerFunc, 1);

    OS_TASK_CREATE(&TCBButton,  "taskButton", 50, taskButton, StackButton);
    OS_TASK_CREATE(&TCBMenu,    "taskMenu",   50, taskMenu,   StackMenu);
    OS_TASK_CREATE(&TCBLed,     "taskLed",    50, taskLed,    StackLed);
}
