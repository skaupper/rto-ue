#include "BSP.h"


//
// Defines
//

//****** GPIO base addresses ******************************

#define GPIOA_BASE_ADDR           ((unsigned int)0x40020000)
#define GPIOC_BASE_ADDR           ((unsigned int)0x40020800)

//****** GPIO defines **************************************

#define GPIOA_MODER               (*(volatile unsigned int*)(GPIOA_BASE_ADDR + 0x00))
#define GPIOA_ODR                 (*(volatile unsigned int*)(GPIOA_BASE_ADDR + 0x14))
#define GPIOA_BSRR                (*(volatile unsigned int*)(GPIOA_BASE_ADDR + 0x18))

#define GPIOC_MODER               (*(volatile unsigned int*)(GPIOC_BASE_ADDR + 0x00))
#define GPIOC_IDR                 (*(volatile unsigned int*)(GPIOC_BASE_ADDR + 0x10))

//****** RCC defines **************************************

#define RCC_BASE_ADDR             ((unsigned int)(0x40023800))
#define RCC_AHB1RSTR              (*(volatile unsigned int*)(RCC_BASE_ADDR + 0x10))
#define RCC_AHB1ENR               (*(volatile unsigned int*)(RCC_BASE_ADDR + 0x30))

#define RCC_LEDPORT_RSTR          RCC_AHB1RSTR
#define RCC_LEDPORT_ENR           RCC_AHB1ENR
#define RCC_LEDPORT_BIT           (0)

#define RCC_BUTTONPORT_RSTR       RCC_AHB1RSTR
#define RCC_BUTTONPORT_ENR        RCC_AHB1ENR
#define RCC_BUTTONPORT_BIT        (2)

//****** LED and Button defines ***************************

#define LED_PORT_MODER            GPIOA_MODER
#define LED_PORT_ODR              GPIOA_ODR
#define LED_PORT_BSRR             GPIOA_BSRR

#define LED0_BIT                  (5)

#define BUTTON_PORT_MODER         GPIOC_MODER
#define BUTTON_PORT_IDR           GPIOC_IDR
#define USERB1_BIT                (13)

//*********************************************************


//
// Global functions
//

void BSP_Init(void) {
    // enable clock for GPIOA
    RCC_LEDPORT_ENR     &= ~(1uL << RCC_LEDPORT_BIT);
    RCC_LEDPORT_RSTR    &= ~(1uL << RCC_LEDPORT_BIT);
    RCC_LEDPORT_ENR     |=  (1uL << RCC_LEDPORT_BIT);

    // initialize LED pin as output
    LED_PORT_MODER      &= ~(3uL << (LED0_BIT * 2));    // Reset mode; sets port to input
    LED_PORT_MODER      |=  (1uL << (LED0_BIT * 2));    // Set to output mode
    LED_PORT_BSRR        =  (0x10000uL << LED0_BIT);    // Initially clear LEDs

    // enable clock for GPIOC
    RCC_BUTTONPORT_ENR  &= ~(1uL << RCC_BUTTONPORT_BIT);
    RCC_BUTTONPORT_RSTR &= ~(1uL << RCC_BUTTONPORT_BIT);
    RCC_BUTTONPORT_ENR  |=  (1uL << RCC_BUTTONPORT_BIT);

    // initialize button as input
    BUTTON_PORT_MODER   &= ~(3uL << (USERB1_BIT * 2));  // Reset mode; sets port to input
}

void BSP_SetLED(void) {
    LED_PORT_BSRR = (1uL << LED0_BIT);              // Turn on LED0
}

void BSP_ClrLED(void) {
    LED_PORT_BSRR = (0x10000uL << LED0_BIT);        // Turn off LED0
}

void BSP_ToggleLED(void) {
    if ((LED_PORT_ODR & (1uL << LED0_BIT)) == 0) {  // LED is turned off
        LED_PORT_BSRR = (1uL << LED0_BIT);          // Turn on LED0
    } else {
        LED_PORT_BSRR = (0x10000uL << LED0_BIT);    // Turn off LED0
    }
}

state_button_t BSP_GetButton(void)
{
    // check if button is pressed using the input data register
    // NOTE: buttons are active low
    if ((BUTTON_PORT_IDR & (1uL << USERB1_BIT)) == 0) {
        return BUTTON_PRESSED;
    } else {
        return BUTTON_RELEASED;
    }
}
