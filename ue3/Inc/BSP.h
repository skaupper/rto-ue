#ifndef BSP_H
#define BSP_H


// In order to avoid warnings for undefined parameters
#ifndef BSP_USE_PARA
  #if defined(NC30) || defined(NC308)
    #define BSP_USE_PARA(para)
  #else
    #define BSP_USE_PARA(para) para=para
  #endif
#endif


#ifdef __cplusplus
extern "C" {
#endif

//
// Type declarations
//

typedef enum {
    BUTTON_PRESSED,
    BUTTON_RELEASED
} state_button_t;


//
// Prototypes
//

/*
 * Initialize LED and Button pins before first use
 */
void BSP_Init                (void);

/*
 * Turns on the LED
 */
void BSP_SetLED              (void);

/*
 * Turns off the LED
 */
void BSP_ClrLED              (void);

/*
 * Toggles the LED state.
 * If the LED is already turned on, this functions turns it off and vice versa.
 */
void BSP_ToggleLED           (void);

/*
 * Queries the current state of the button using state_button_t to represent the state;
 */
state_button_t BSP_GetButton (void);

#ifdef __cplusplus
}
#endif

#endif
